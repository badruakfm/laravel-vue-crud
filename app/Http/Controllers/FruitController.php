<?php

namespace App\Http\Controllers;

use App\FruitModel;
use Illuminate\Http\Request;

class FruitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = FruitModel::all();

        return response()->json(compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => ['required'],
            'harga' => ['required'],
            'gizi' => ['required']
        ]);
        $data = FruitModel::create($request->all());
        return response()->json(compact('data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = FruitModel::find($id);

        return response()->json(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => ['required'],
            'harga' => ['required'],
            'gizi' => ['required']
        ]);
        FruitModel::where('id', $id)->update([
            'nama' => $request->nama,
            'harga' => $request->harga,
            'gizi' => $request->gizi
        ]);
    }

    public function kurangiStok($id)
    {
        FruitModel::where('id',$id)->update([
            'stok' => FruitModel::find($id)->stok - 1
        ]);
    }
    public function tambahStok($id)
    {
        FruitModel::where('id',$id)->update([
            'stok' => FruitModel::find($id)->stok + 1
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        FruitModel::destroy($id);

        return response()->json([
            'data' => 'terhapus'
        ]);
    }
}