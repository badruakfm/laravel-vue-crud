<?php

Route::get('buah','FruitController@index');
Route::post('buah','FruitController@store');
Route::get('buah/{id}','FruitController@show');
Route::put('buah/{id}','FruitController@update');
Route::put('buah/kurang-stok/{id}','FruitController@kurangiStok');
Route::put('buah/tambah-stok/{id}','FruitController@tambahStok');
Route::delete('buah/{id}','FruitController@destroy');