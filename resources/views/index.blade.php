<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h3>Buah - Buahan</h3>

    <div id="app">
        <h4>Tambah Buah</h4>
        <input type="text" v-model="nama" placeholder="Nama Buah"> <br>
        <input type="text" v-model="harga" placeholder="Harga"> <br>
        <input type="text" v-model="gizi" placeholder="Kandungan Gizi" @keyup.enter="tambahBuah"> <br>
        <h4>Daftar Buah</h4>
        <table border="1">
            <tr>
                <th>Nama Buah</th>
                <th>Harga</th>
                <th>Kandungan Gizi</th>
                <th>Stok</th>
                <th>Aksi</th>
            </tr>
            <tr v-for="(bua, index) in buah">
                <td>@{{bua.nama}} </td>
                <td>@{{bua.harga}} </td>
                <td>@{{bua.gizi}} </td>
                <td>@{{bua.stok}} </td>
                <th>
                    <button @click="kurangiStok(bua.id, index)">-</button>
                    <button @click="tambahStok(bua.id, index)">+</button>
                    <button @click="hapusBuah(bua.id, index)">X</button>
                </th>
            </tr>
        </table>

    </div>

    <script src="{{ asset('./js/vue.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>
    <script>
        const vm = new Vue({
            el: '#app',
            data: {
                nama: '',
                harga: 0,
                gizi: '',
                stok: 0,
                buah: [],
                tambahShow: false
            },
            methods: {
                tambahBuah: function() {
                    this.$http.post('http://127.0.0.1:8000/api/buah', {
                        'nama': this.nama,
                        'harga': this.harga,
                        'gizi': this.gizi,
                        'stok': 5,
                    }).then(response => {
                        this.buah.push({
                            'id' : response.body.data.id,
                            'nama': this.nama,
                            'harga': this.harga,
                            'gizi': this.gizi,
                            'stok': 5,
                        })
                        this.nama = ''
                        this.harga = 0
                        this.gizi = ''
                        this.stok = 0

                    });


                },
                kurangiStok: function(id, i) {
                    this.$http.put('http://127.0.0.1:8000/api/buah/kurang-stok/' + id, {}).then(response => {
                        this.buah[i].stok--;
                    })
                },
                tambahStok: function(id, i) {
                    this.$http.put('http://127.0.0.1:8000/api/buah/tambah-stok/' + id, {}).then(response => {
                        this.buah[i].stok++;
                    })
                },
                hapusBuah: function(id, i) {
                    this.$http.delete(`http://127.0.0.1:8000/api/buah/${id}`).then(response => {
                        this.buah.splice(i, 1)
                        let result = response.body.data
                        console.log(result)
                    })
                    
                }
            },
            mounted: function() {
                this.$http.get('http://127.0.0.1:8000/api/buah').then(response => {
                    let result = response.body.data
                    this.buah = result
                })
            }
        });
    </script>
</body>

</html>